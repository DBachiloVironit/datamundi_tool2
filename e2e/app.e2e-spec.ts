import { DatamundiTool2Page } from './app.po';

describe('datamundi-tool2 App', () => {
  let page: DatamundiTool2Page;

  beforeEach(() => {
    page = new DatamundiTool2Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
