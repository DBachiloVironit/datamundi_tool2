import {Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {MainComponentGuard} from './services/guards/main.guard';
import {HtmlPairToolComponent} from './html-pair-tool/html-pair-tool.component';


export const AppRoutes: Routes = [{
  path: '',
  canActivate: [MainComponentGuard],
  component: HtmlPairToolComponent
}, {
  path: 'login',
  component: LoginComponent
}, {
  path: '**',
  redirectTo: 'session/404'
}];
