import {Component, ElementRef, HostListener, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MdSnackBar, MdSnackBarConfig} from '@angular/material';
import {Router} from '@angular/router';
import {JobsApi} from '../services/api/jobs.api.service';
import {AuthService} from '../services/auth.service';
import {IErrorInfo} from '../services/error-code-processor.service';
import {IRequestResult} from '../shared/RequestResult';
import {Guid} from '../shared/tools/Guid';

@Component({
  selector: 'app-url-pair-tool',
  templateUrl: './html-pair-tool.component.html',
  styleUrls: ['./html-pair-tool.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HtmlPairToolComponent implements OnInit {

  @ViewChild('source', {read: ElementRef}) public source: ElementRef;
  @ViewChild('translation', {read: ElementRef}) public translation: ElementRef;

  colors = ['yellow', 'red', 'green', 'grey', 'light-blue'];
  activeColor = 0;
  tagsWhiteList = ['ol', 'ul', 'li', 'p', 'b', 'i', 'u', 'span', 'a'];
  tagsBlackList = ['mark', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'code', 'pre'];
  punctuationMarks = ['.', ',', ':', ';', '!', '?', '[', ']', '(', ')', '{', '}', '\'', '"', '-', '�', '�', '@', '#', '^', '*', '_', '+', '='];

  pairGuid: string;
  sourceSelectionState = false;
  translationSelectionState = false;
  sourceGuidStack = [];
  translationGuidStack = [];

  fuid: string;
  job: string;
  pairId: number;
  jobDone = false;

  enterTime = 0;
  leaveTime = 0;
  mouseMiles: number;
  scrollStateSource: number;
  scrollStateTranslation: number;

  jobName = 'Sentence Alignment Evaluation: Token Pair Markup';
  mouseLastPosition = {x: null, y: null};

  pair = {
    source: {
      text: '',
      lang: ''
    },
    translation: {
      text: '',
      lang: ''
    }
  };

  errors = {
    no_punctuation: 'Punctuation marks are not allowed to be in the selection.',
    wrong_word_count: 'You must select 4-8 words (8-16 characters for Chinese).',
    tag_intersection: 'HTML tags intersection occured.',
    no_pair: 'No pair selection specified.',
    job_done: 'No more selections are allowed. Please click "Next" to proceed to the next pair.',
    cannot_undo: 'Cannot perform undo. No pair selection undone.'
  };

  meta: any = {};

  constructor(private snackBar: MdSnackBar, private jobsApi: JobsApi, private authService: AuthService) {
  }

  @HostListener('document:mousemove', ['$event'])
  public onMouseMove(e: any): void {
    if (this.mouseLastPosition.x) {
      this.mouseMiles += Math.sqrt(Math.pow(this.mouseLastPosition.y - e.clientY, 2) + Math.pow(this.mouseLastPosition.x - e.clientX, 2));
    }

    this.mouseLastPosition.x = e.clientX;
    this.mouseLastPosition.y = e.clientY;
  }

  ngOnInit() {
    this.fuid = localStorage.getItem('fuid');
    this.job = localStorage.getItem('job');
    this.loadMeta();
    this.getUserState();
  }

  public getUserState(): void {
    this.jobsApi.getHtmlJobState(this.fuid, this.job)
      .subscribe((response: IRequestResult): void => {
        if (response.code === 'job_done') {
          this.jobDone = true;
          // TODO: show modal invoice page
        } else {
          this.resetData(response.data);
        }
      }, (error: IErrorInfo): void => {
        if (error.code === 'bad_fuid' || error.code === 'bad_job_name') {
          this.authService.logout();
        }
        this.showError(error.errorMessage);
      });
  }

  public loadNewJobHandler(): void {
    this.authService.logout();
  }

  public resetData(data): void {
    this.pairId = data.pair.PairID;
    this.pair = {
      source: {
        text: data.pair.source,
        lang: data.pair.srcLang
      },
      translation: {
        text: data.pair.translation,
        lang: data.pair.transLang
      }
    };
    this.enterTime = new Date().getTime();
    this.mouseMiles = 0;
    this.scrollStateSource = 0;
    this.scrollStateTranslation = 0;

    this.source.nativeElement.scrollTop = 0;
    this.translation.nativeElement.scrollTop = 0;

    this.pairGuid = Guid.newGuid();
    this.sourceSelectionState = false;
    this.translationSelectionState = false;
    this.activeColor = 0;

    setTimeout(() => {
      this.processDom('source', this.tagsWhiteList.join(', '), 'select');
      this.processDom('translation', this.tagsWhiteList.join(', '), 'select');
    }, 1000);
  }

  private processDom(targetId: string, childSelector: string, className: string): void {
    const nodes = document.getElementById(targetId).querySelectorAll(childSelector);
    for (let i = 0; i < nodes.length; i++) {
      nodes[i].className = className;
    }
  }

  public loadMeta(): void {
    this.jobsApi.getJobMeta(this.job)
      .subscribe((response: IRequestResult): void => {
        this.meta = response.data.meta;
      }, (error: IErrorInfo): void => {
        this.showError(error.errorMessage);
      });
  }

  public nextHandler(): void {
    if (!this.jobDone) {
      this.leaveTime = new Date().getTime();
      const evaluations = {
        enterTime: this.enterTime,
        leaveTime: this.leaveTime,
        mouseMiles: this.mouseMiles,
        scrollStateSource: this.scrollStateSource,
        scrollStateTranslation: this.scrollStateTranslation,
        sourceHtml: this.source.nativeElement.innerHTML,
        translationHtml: this.translation.nativeElement.innerHTML
      };

      this.jobsApi.submitNext(this.fuid, this.job, this.pairId, evaluations)
        .subscribe((response: IRequestResult): void => {
          this.getUserState();
        }, (error: IErrorInfo): void => {
          this.showError(error.errorMessage);
        });
    }
  }

  public sourceScroll(event: any): void {
    const scrollState = event.target.scrollTop / event.target.scrollTopMax * 100;
    if (scrollState > this.scrollStateSource) {
      this.scrollStateSource = scrollState;
    }
  }

  public translationScroll(event: any): void {
    const scrollState = event.target.scrollTop / event.target.scrollTopMax * 100;
    if (scrollState > this.scrollStateTranslation) {
      this.scrollStateTranslation = scrollState;
    }
  }

  public sourceHighlight(): void {
    if (this.sourceSelectionState && !this.translationSelectionState) {
      this.showError(this.errors.no_pair);
      this.clearSelection();
      return;
    }

    if (this.handleHighlight()) {
      this.sourceGuidStack.push(this.pairGuid);
      this.sourceSelectionState = true;
      this.postHighlight();
    }
  }

  public translationHighlight(): void {
    if (this.translationSelectionState && !this.sourceSelectionState) {
      this.showError(this.errors.no_pair);
      this.clearSelection();
      return;
    }

    if (this.handleHighlight()) {
      this.translationGuidStack.push(this.pairGuid);
      this.translationSelectionState = true;
      this.postHighlight();
    }
  }

  private postHighlight(): void {
    if (this.sourceSelectionState && this.translationSelectionState) {
      this.sourceSelectionState = false;
      this.translationSelectionState = false;
      this.pairGuid = Guid.newGuid();
      this.activeColor++;
    }
  }

  public handleHighlight(): boolean {
    if (this.activeColor === 5) {
      this.showError(this.errors.job_done);
      this.clearSelection();
      return false;
    }

    const selection = window.getSelection();
    if (selection.rangeCount === 0) {
      return false;
    }

    const range = selection.getRangeAt(0).cloneRange();
    const text = selection.toString();

    if (range.endOffset - range.startOffset <= 0) {
      return false;
    }

    for (let char = 0; char < text.length; char++) {
      if (this.punctuationMarks.indexOf(text[char]) !== -1) {
        this.showError(this.errors.no_punctuation);
        this.clearSelection();
        return false;
      }
    }

    const word_count = text.split(' ').length;
    if (this.inRange(word_count, 4, 8) || (this.pair.translation.lang === 'CN' && this.inRange(text.length, 8, 16))) {
      const mark = document.createElement('mark');
      mark.className = 'no-select mat-' + this.colors[this.activeColor];
      mark.setAttribute('guid', this.pairGuid);
      mark.setAttribute('uts', Math.floor(new Date().getTime() / 1000).toString());

      try {
        range.surroundContents(mark);
      } catch (e) {
        this.showError(this.errors.tag_intersection);
        this.clearSelection();
        return false;
      }

      selection.removeAllRanges();
      selection.addRange(range);
      this.clearSelection();
    } else {
      this.showError(this.errors.wrong_word_count);
      this.clearSelection();
      return false;
    }

    return true;
  }

  private clearSelection(): void {
    if (window.getSelection) {
      window.getSelection().removeAllRanges();
    }
  }

  private inRange(value, min, max) {
    return value >= min && value <= max;
  }

  public undoSourceSelection(): void {
    if (!this.sourceSelectionState && this.translationSelectionState) {
      this.showError(this.errors.cannot_undo);
      return;
    }

    const guid = this.sourceGuidStack.pop();
    const element = document.getElementById('source').querySelector(`[guid="${guid}"`);
    this.removeTag(element);
    this.translationSelectionState = this.activeColor > 0;
    this.postUndo();
  }

  public undoTranslationSelection(): void {
    if (this.sourceSelectionState && !this.translationSelectionState) {
      this.showError(this.errors.cannot_undo);
      return;
    }

    const guid = this.translationGuidStack.pop();
    const element = document.getElementById('translation').querySelector(`[guid="${guid}"`);
    this.removeTag(element);
    this.sourceSelectionState = this.activeColor > 0;
    this.postUndo();
  }

  private postUndo(): void {
    if ((!this.sourceSelectionState || !this.translationSelectionState) && this.activeColor > 0) {
      this.activeColor--;
    } else if (!this.sourceSelectionState && !this.translationSelectionState && this.activeColor > 0) {
      this.sourceSelectionState = true;
      this.translationSelectionState = true;
    } else {
      this.sourceSelectionState = false;
      this.translationSelectionState = false;
    }
  }

  private removeTag(tag: any): void {
    const parent = tag.parentNode;
    while (tag.firstChild) {
      parent.insertBefore(tag.firstChild, tag);
    }
    parent.removeChild(tag);
  }

  private showError(message: string): void {
    const snackBarRef = this.snackBar.open(`${message}`, 'Close', {
      duration: 3000
    } as MdSnackBarConfig);
    snackBarRef.onAction().subscribe(() => {
      snackBarRef.dismiss();
    });
  }

}
