import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlPairToolComponent } from './html-pair-tool.component';

describe('HtmlPairToolComponent', () => {
  let component: HtmlPairToolComponent;
  let fixture: ComponentFixture<HtmlPairToolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlPairToolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlPairToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
