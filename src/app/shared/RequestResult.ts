import {Injectable} from '@angular/core';

export interface IRequestResult {
  code: string;
  data: any;
  statusCode: number;
}
