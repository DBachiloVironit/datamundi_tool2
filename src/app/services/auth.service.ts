import {Injectable} from '@angular/core';
import {Headers} from '@angular/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {IRequestResult} from '../shared/RequestResult';

import {AuthApi} from './api/auth.api.service';
import {IErrorInfo} from './error-code-processor.service';


@Injectable()
export class AuthService {

  static get FUID(): string {
    return JSON.parse(localStorage.getItem('fuid'));
  }

  constructor(private router: Router, private authApiService: AuthApi) {
    //
  }

  public logout(): void {
    localStorage.removeItem('fuid');
    localStorage.removeItem('job');
    this.router.navigate(['/login']);
  }

  public login(email: string, pin: string, job: string): Observable<string | IErrorInfo> {
    const body = {email, pin, job};
    const headers: Headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    return Observable.create((observer: Observer<any>) => {
      this.authApiService.login(body).subscribe((data: IRequestResult) => {
        localStorage.setItem('fuid', data.data.fuid);
        localStorage.setItem('job', job);
        observer.next(data);
      }, (error: IErrorInfo | any) => {
        observer.error(error);
      });
    });
  }

}
