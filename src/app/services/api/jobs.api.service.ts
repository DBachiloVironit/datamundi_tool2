import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';
import {IRequestResult} from '../../shared/RequestResult';
import {ErrorCodeProcessor, IErrorInfo} from '../error-code-processor.service';
import {BaseHttp} from './baseHttp.service';

@Injectable()
export class JobsApi extends BaseHttp {
  private jobsUrl: string = environment.server + '/api/jobs';
  private htmlToolUrl: string = environment.server + '/api/tools/html';

  constructor(protected http: Http, protected errorCodeProcessor: ErrorCodeProcessor, protected router: Router) {
    super(http, errorCodeProcessor, router);
  }

  public getJobsList(): Observable<IRequestResult | IErrorInfo> {
    return this.get(this.jobsUrl);
  }

  public getHtmlJobState(fuid: string, job: string): Observable<IRequestResult | IErrorInfo> {
    return this.get(this.htmlToolUrl + `/state?fuid=${fuid}&job=${job}`);
  }

  public getJobMeta(job: string): Observable<IRequestResult | IErrorInfo> {
    return this.get(this.jobsUrl + `/meta?job=${job}`);
  }

  public submitNext(fuid: string, job: string, pairId: number, evaluations: any): Observable<IRequestResult | IErrorInfo> {
    const body = {fuid, job, pairId, evaluations};
    return this.post(this.htmlToolUrl + '/next', body);
  }
}
