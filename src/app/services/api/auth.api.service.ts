import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';
import {IRequestResult} from '../../shared/RequestResult';
import {ErrorCodeProcessor} from '../error-code-processor.service';
import {BaseHttp} from './baseHttp.service';

@Injectable()
export class AuthApi extends BaseHttp {
  private loginUrl: string = environment.server + '/api/login';

  constructor(protected http: Http, protected errorProcessor: ErrorCodeProcessor, protected router: Router) {
    super(http, errorProcessor, router);
  }

  public login(body: any): Observable<IRequestResult> {
    return this.http.post(this.loginUrl, body)
      .map((res: Response): any => this.extractData(res))
      .catch((error: Response): any => this.handleError(error));
  }
}
