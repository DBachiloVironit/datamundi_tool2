import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class MainComponentGuard implements CanActivate {

  constructor(private router: Router) {
    //
  }

  canActivate(): Observable<boolean> | boolean {
    if (localStorage.getItem('fuid') && localStorage.getItem('job')) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }

}
