import {NgModule} from '@angular/core';
import {AuthApi} from './api/auth.api.service';
import {AuthService} from './auth.service';
import {MainComponentGuard} from './guards/main.guard';
import {ErrorCodeProcessor} from './error-code-processor.service';

@NgModule({
  imports: [],
  declarations: [],
  exports: [],
  providers: [
    AuthApi,
    AuthService,
    ErrorCodeProcessor,
    MainComponentGuard
  ]
})
export class ServicesModule {
}
