import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

export interface IErrorInfo {
  code: string;
  statusCode: number;
  errorMessage: string;
}

@Injectable()
export class ErrorCodeProcessor {
  private errorInfoDataSet: any = {
    error: 'Try again later.'
  };

  constructor(private http: Http) {
    this.http.get('assets/data/errors.json')
      .map((res: any) => res.json())
      .subscribe((data: any) => {
        this.errorInfoDataSet = data;
      });
  }

  public getMessageByErrorCode(errorCode: string): string {
    if (errorCode && this.errorInfoDataSet[errorCode]) {
      return this.errorInfoDataSet[errorCode];
    }
    return 'Error connecting with server.';
  }
}
